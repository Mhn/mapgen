#pragma once

#include <numbers>

namespace gis_cpp 
{
  struct GeodeticCoord
  {
    double latitude;
    double longitude;
  };

  struct Ellipsoid
  {
    double flattening;
    double equatorialRadius;

    constexpr double e2() const
    {
      return flattening * (2 - flattening); // e2: first eccentricity squared
    }
    constexpr double n() const
    {
      return flattening / (2 - flattening); // n: 3rd flattening
    }
    constexpr double rectifyingRadius() const
    {
      return equatorialRadius / (1 + n()) * (1 + 0.25 * std::pow(n(), 2) + 0.015625 * std::pow(n(), 4));
    }
  };

  struct Projection
  {
    Ellipsoid ellipsoid;
    double centralMeridian;
    double scale;
    double falseNorthing;
    double falseEasting;
  };

  template <Projection P>
  struct GridCoord
  {
    double northing;
    double easting;
  };

  template <Projection P>
  struct Bounds
  {
    GridCoord<P> min;
    GridCoord<P> max;
  };

  template <Projection P>
  struct Tile
  {
    int row;
    int column;
    unsigned int zoom;
  };

  namespace projection
  {

    template <Projection P>
    constexpr GridCoord<P> geodeticToGrid(GeodeticCoord coord)
    {
        const double e2 = P.ellipsoid.e2();
        const double n = P.ellipsoid.n();
        const double rectifyingRadius = P.ellipsoid.rectifyingRadius();

        double A = e2;
        double B = (5 * std::pow(e2, 2) - std::pow(e2, 3)) / 6.0;
        double C = (104 * std::pow(e2, 3) - 45 * std::pow(e2, 4)) / 120.0;
        double D = (1237 * std::pow(e2, 4)) / 1260.0;

        // Latitude and longitude are expected to be given in degrees
        // phi and lambda: latitude and longitude in radians
        double phi = coord.latitude * std::numbers::pi / 180;
        double lambda = coord.longitude * std::numbers::pi / 180;
        double lambda0 = P.centralMeridian * std::numbers::pi / 180;

        // deltaLambda: longitude relative to the central meridian
        double deltaLambda = lambda - lambda0;

        // phiStar: conformal latitude
        double phiStar =
                phi - std::sin(phi) * std::cos(phi) *
                (A + B*std::pow(std::sin(phi), 2) + C*std::pow(std::sin(phi), 4) + D*std::pow(std::sin(phi), 6));

        double xiPrim = std::atan(tan(phiStar) / std::cos(deltaLambda));
        double etaPrim = atanh(std::cos(phiStar) * std::sin(deltaLambda));

        double beta1 = 1/2.0 * n - 2/3.0 * std::pow(n, 2) + 5/16.0 * std::pow(n, 3)     + 41/180.0 * std::pow(n, 4);
        double beta2 =           13/48.0 * std::pow(n, 2)  - 3/5.0 * std::pow(n, 3)   + 557/1440.0 * std::pow(n, 4);
        double beta3 =                                    61/240.0 * std::pow(n, 3)    - 103/140.0 * std::pow(n, 4);
        double beta4 =                                                              49561/161280.0 * std::pow(n, 4);

        auto northing = P.falseNorthing
                + P.scale * rectifyingRadius * (xiPrim
                                                + beta1 * std::sin(2*xiPrim) * std::cosh(2*etaPrim)
                                                + beta2 * std::sin(4*xiPrim) * std::cosh(4*etaPrim)
                                                + beta3 * std::sin(6*xiPrim) * std::cosh(6*etaPrim)
                                                + beta4 * std::sin(8*xiPrim) * std::cosh(8*etaPrim));
        auto easting = P.falseEasting
                + P.scale * rectifyingRadius * (etaPrim
                                                + beta1 * std::cos(2*xiPrim) * std::sinh(2*etaPrim)
                                                + beta2 * std::cos(4*xiPrim) * std::sinh(4*etaPrim)
                                                + beta3 * std::cos(6*xiPrim) * std::sinh(6*etaPrim)
                                                + beta4 * std::cos(8*xiPrim) * std::sinh(8*etaPrim));
        return { northing, easting };
    }

    template <Projection P>
    constexpr GeodeticCoord gridToGeodetic(GridCoord<P> coord)
    {
        const double e2 = P.ellipsoid.e2();
        const double n = P.ellipsoid.n();
        const double rectifyingRadius = P.ellipsoid.rectifyingRadius();

        double xi = (coord.northing - P.falseNorthing) / (P.scale * rectifyingRadius);
        double eta = (coord.easting - P.falseEasting) / (P.scale * rectifyingRadius);

        double delta1 = 1/2.0 * n - 2/3.0 * std::pow(n, 2) + 37/96.0 * std::pow(n, 3)     - 1/360.0 * std::pow(n, 4);
        double delta2 =            1/48.0 * std::pow(n, 2)  + 1/15.0 * std::pow(n, 3)  - 437/1440.0 * std::pow(n, 4);
        double delta3 =                                     17/480.0 * std::pow(n, 3)    - 37/840.0 * std::pow(n, 4);
        double delta4 =                                                               4397/161280.0 * std::pow(n, 4);

        double xiPrim = xi
                - delta1 * std::sin(2*xi) * std::cosh(2*eta)
                - delta2 * std::sin(4*xi) * std::cosh(4*eta)
                - delta3 * std::sin(6*xi) * std::cosh(6*eta)
                - delta4 * std::sin(8*xi) * std::cosh(8*eta);
        double etaPrim = eta
                - delta1 * std::cos(2*xi) * std::sinh(2*eta)
                - delta2 * std::cos(4*xi) * std::sinh(4*eta)
                - delta3 * std::cos(6*xi) * std::sinh(6*eta)
                - delta4 * std::cos(8*xi) * std::sinh(8*eta);

        double phiStar = std::asin(std::sin(xiPrim) / std::cosh(etaPrim)); // Conformal latitude
        double deltaLambda = std::atan(std::sinh(etaPrim) / std::cos(xiPrim));

        double AStar =  e2     + std::pow(e2, 2)       + std::pow(e2, 3)        + std::pow(e2, 4);
        double BStar =      (7 * std::pow(e2, 2)  + 17 * std::pow(e2, 3)   + 30 * std::pow(e2, 4)) / -6;
        double CStar =                            (224 * std::pow(e2, 3)  + 889 * std::pow(e2, 4)) / 120;
        double DStar =                                                    (4279 * std::pow(e2, 4)) / -1260;

        double phi = phiStar
                + std::sin(phiStar) * std::cos(phiStar) * (  AStar
                                                 + BStar * std::pow(std::sin(phiStar), 2)
                                                 + CStar * std::pow(std::sin(phiStar), 4)
                                                 + DStar * std::pow(std::sin(phiStar), 6));

        // phi: latitude in radians, lambda: longitude in radians
        // Return latitude and longitude as degrees
        return {
          .latitude = phi * 180.0 / std::numbers::pi,
          .longitude = P.centralMeridian + deltaLambda * 180.0 / std::numbers::pi
        };
    }
  }

  namespace tiling
  {
    template <Projection P>
    constexpr Tile<P> gridToTile(GridCoord<P> coord, unsigned int zoom)
    {
      GridCoord<P> origin{-P.falseNorthing, -P.falseEasting};
//      auto origin = CS.bounds.min;
//      auto scaleNorthing = 1.0 / (CS.bounds.max.northing - CS.bounds.min.northing);
//      auto scaleEasting = 1.0 / (CS.bounds.max.easting - CS.bounds.min.easting);
//      auto scale = std::min(scaleNorthing, scaleEasting);
      auto scale = 1.0 / (P.ellipsoid.equatorialRadius * 2.0 * std::numbers::pi);
      auto n = std::pow(2, zoom);
//
//      std::cout << "scaleNorthing " << scaleNorthing << " scaleEasting " << scaleEasting << " n " << n << "\n";
//      std::cout << "(coord.northing - origin.northing) " << (coord.northing - origin.northing) << " scaleNorthing * (coord.northing - origin.northing) " << scaleNorthing * (coord.northing - origin.northing) << "\n";
//      std::cout << "(coord.easting - origin.easting) " << (coord.easting - origin.easting) << " scaleEasting * (coord.easting - origin.easting) " << scaleEasting * (coord.easting - origin.easting) << "\n";
      return {
        .row = static_cast<int>(std::floor(n * scale * (coord.northing - origin.northing))),
        .column = static_cast<int>(std::floor(n * scale * (coord.easting - origin.easting))),
        .zoom = zoom
      };
    }

    template <Projection P>
    constexpr GridCoord<P> tileLowerCorner(Tile<P> tile)
    {
      GridCoord<P> origin{-P.falseNorthing, -P.falseEasting};
//      auto origin = CS.bounds.min;
//      auto scaleNorthing = CS.bounds.max.northing - CS.bounds.min.northing;
//      auto scaleEasting = CS.bounds.max.easting - CS.bounds.min.easting;
//      auto scale = std::max(scaleNorthing, scaleEasting);
      auto scale = P.ellipsoid.equatorialRadius * 2.0 * std::numbers::pi;
      auto n = std::pow(2, tile.zoom);
      return {
        .northing = tile.row / n * scale + origin.northing,
        .easting = tile.column / n * scale + origin.easting
      };
    }

    template <Projection P>
    constexpr Bounds<P> tileBounds(Tile<P> tile)
    {
      return {
        .min = tileLowerCorner(tile),
        .max = tileLowerCorner<P>({ .row = tile.row + 1, .column = tile.column + 1, .zoom = tile.zoom })
      };
    }
  }
}

//constexpr auto GRS80 = gis_cpp::Ellipsoid{ .flattening = 1.0 / 298.257222101, .equatorialRadius = 6378137.0 };
//constexpr auto SWEREF99TM = gis_cpp::Projection{ .ellipsoid = GRS80, .centralMeridian = 15.0, .scale = 0.9996, .falseNorthing = 0.0, .falseEasting = 500000.0 };

//constexpr auto SWEREF99TM_TMS = gis_cpp::CoordinateSystem<SWEREF99TM>{ .bounds. scale = { .northing = 15.0, .easting = 0.9996 }, .origin = { .northing = 0.0, .easting = 500000 } };
