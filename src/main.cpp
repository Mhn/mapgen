#include <ogrsf_frmts.h>
#include <gdal_alg.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <array>
#include <numbers>
#include <functional>
#include <filesystem>
#include <png++/png.hpp>
#include "gausskruger.hpp"

enum class BaseType : uint8_t
{
  Unknown,
  Water,
  WaterContour,
  OpenArea,
  Forest,
  Urban,
  Industrial,
  Glacier,
};

enum class FeatureType : uint8_t
{
  None,
  Trail,
  Gravel,
  Small,
  Small2,
  Big,
  Big2,
  Train,
  Building,
  Ferry,
  Tunnel,
  NatureReserve,
  Forbidden,
  ContourLine,
  Swamp,
  Powerline,
};

struct Pixel
{
  BaseType baseType : 3;
  bool text : 1;
  FeatureType featureType : 4;
};

static_assert(sizeof(Pixel) == sizeof(uint8_t));

template <typename T>
struct Layer
{
  std::string file;
  std::string query;
  T type;
};

constexpr auto build_query(std::string_view str, std::string_view where = {})
{
  auto prefixes = std::to_array<std::string_view>(
    {
      "K23", "K24", "K31", "K32", "K33", "K34", "K42", "K44", "L23", "L24", "L31", "L32", "L33", "L34", "L41", "L42", "L43", "L44", "L51", "L52", "M31", "M32", "M33", "M34", "M41", "M42", "M43", "M44", "M51", "M52", "M53", "M54", "N31", "N32", "N33", "N34", "N41", "N42", "N43", "N44", "N51", "N52", "N53", "N54", "N61", "N62", "P31", "P32", "P33", "P34", "P41", "P42", "P43", "P44", "P51", "P52", "P53", "P54", "P61", "P62", "Q31", "Q33", "Q34", "Q41", "Q42", "Q43", "Q44", "Q51", "Q52", "Q53", "Q54", "R33", "R41", "R42", "R43", "R44", "R51", "R52", "R53", "R54", "S41", "S42", "S43", "S44", "S51", "S52", "S53", "S54", "T41", "T42", "T43", "T44", "T51", "T52", "T53", "T54", "U41", "U42", "U43", "U44", "U51", "U52", "U53", "U54", "V34", "V41", "V42", "V43", "V44", "V51", "V52", "V53", "W33", "W34", "W41", "W43", "W44", "W51", "W52", "X43", "X51", "X52",
    });

  std::string res;

  std::size_t i = 0;
  for (auto & prefix : prefixes)
  {
    res += "SELECT * FROM ";
    res += prefix;
    res += "_";
    res += str;
    if (!where.empty())
    {
      res += " ";
      res += where;
    }
    if (++i < prefixes.size())
      res += " UNION ALL ";
  }
  return res;
}

auto baseLayers = std::to_array<Layer<BaseType>>({
//  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM mark WHERE objekttypnr IN (2648)", BaseType::Unknown },
  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM mark WHERE objekttypnr IN (2642, 2643, 2640, 2644, 2635)", BaseType::OpenArea },
  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM mark WHERE objekttypnr IN (2645, 2646, 2647)", BaseType::Forest },
  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM mark WHERE objekttypnr IN (2636, 2637, 2638)", BaseType::Urban },
  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM mark WHERE objekttypnr IN (2639)", BaseType::Industrial },
  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM mark WHERE objekttypnr IN (2631, 2632, 2633, 2634)", BaseType::Water },
  { "../../topology50/hydrografi_sverige.gpkg", "SELECT * FROM hydrolinje WHERE objekttypnr IN (1581, 15811, 15812)", BaseType::WaterContour },
  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM markkantlinje WHERE objekttypnr IN (2612, 2613, 2614, 2615)", BaseType::WaterContour },

  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_omrade WHERE objtype IN ("ÅpentOmråde", "Alpinbakke", "DyrketMark", "Golfbane"))", BaseType::OpenArea },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_omrade WHERE objtype IN ("Skog"))", BaseType::Forest },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_omrade WHERE objtype IN ("BymessigBebyggelse", "SportIdrettPlass", "Tettbebyggelse", "Park"))", BaseType::Urban },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_omrade WHERE objtype IN ("Industriområde", "Lufthavn", "Steinbrudd", "Steintipp"))", BaseType::Urban },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_omrade WHERE objtype IN ("ElvBekk", "FerskvannTørrfall", "Innsjø", "InnsjøRegulert", "Havflate"))", BaseType::Water },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_senterlinje WHERE objtype IN ("ElvBekk"))", BaseType::WaterContour },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_grense WHERE objtype IN ("ElvBekkKant", "Innsjøkant", "InnsjøkantRegulert", "Kystkontur"))", BaseType::WaterContour },

  { "finland.shp.zip", build_query("VesiAlue"), BaseType::Water },
  { "finland.shp.zip", build_query("VesiViiva"), BaseType::WaterContour },
  { "finland.shp.zip", build_query("Maasto1Reuna", R"(WHERE Kohderyhma IN (19))"), BaseType::WaterContour },
  { "finland.shp.zip", build_query("Maasto1Viiva", R"(WHERE Kohderyhma IN (19))"), BaseType::WaterContour },
  { "finland.shp.zip", build_query("HallintoAlue", R"(WHERE Kohderyhma IN (64) AND Kohdeluokk IN (32470))"), BaseType::Industrial },
  { "finland.shp.zip", build_query("HallintoAlue", R"(WHERE Kohderyhma IN (64) AND Kohdeluokk IN (32800, 32900))"), BaseType::OpenArea },
  { "finland.shp.zip", build_query("HallintoAlue", R"(WHERE Kohderyhma IN (70) AND Kohdeluokk IN (39120))"), BaseType::OpenArea },
  });

auto featureLayers = std::to_array<Layer<FeatureType>>({
//  { "../../topology50/mark_sverige.gpkg", "SELECT * FROM sankmark WHERE objekttypnr IN (2651, 2652)", FeatureType::Swamp },
//  { "../../topology50/hojd_sverige.gpkg", R"(SELECT * FROM hojdlinje WHERE objekttypnr IN (2401, 2402, 2404, 2405) AND hojdvarde % 25 = 0)", FeatureType::ContourLine },
//  { "../../topology50/hojd_sverige.gpkg", R"(SELECT * FROM hojdlinje WHERE objekttypnr IN (2401, 2402, 2404, 2405) AND hojdvarde % 100 = 0)", FeatureType::ContourLine },
//  { "../../topology50/byggnadsverk_sverige.gpkg", "SELECT * FROM byggnad WHERE objekttypnr IN (2061, 2052, 2063, 2064, 2065, 2066, 2067)", FeatureType::Building },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM ralstrafik WHERE objekttypnr IN (1861, 1862)", FeatureType::Train },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM ralstrafik WHERE objekttypnr IN (1891)", FeatureType::Ferry },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM vaglinje WHERE objekttypnr IN (1801, 1802, 1803, 1808)", FeatureType::Big2 },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM vaglinje WHERE objekttypnr IN (1804, 1809)", FeatureType::Big },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM vaglinje WHERE objekttypnr IN (1805, 1810)", FeatureType::Small2 },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM vaglinje WHERE objekttypnr IN (1806, 1811)", FeatureType::Small },
  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM vaglinje WHERE objekttypnr IN (1807, 1816)", FeatureType::Gravel },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM ovrig_vag WHERE objekttypnr IN (1628)", FeatureType::Gravel },
//  { "../../topology50/kommunikation_sverige.gpkg", "SELECT * FROM ovrig_vag WHERE objekttypnr IN (1842, 1623, 1624, 1625, 1846, 1847)", FeatureType::Trail },
//
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Arealdekke_omrade WHERE objtype IN ("Myr"))", FeatureType::Swamp },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Høyde_senterlinje WHERE objtype IN ("Forsenkningskurve", "Hjelpekurve", "Høydekurve") AND hoyde % 25 = 0)", FeatureType::ContourLine },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Høyde_senterlinje WHERE objtype IN ("Forsenkningskurve", "Hjelpekurve", "Høydekurve") AND hoyde % 100 = 0)", FeatureType::ContourLine },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_BygningerOgAnlegg_omrade WHERE objtype IN ("Bygning", "Tank"))", FeatureType::Building },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE objtype IN ("Bane"))", FeatureType::Train },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE objtype IN ("Bilferjestrekning", "Passasjerferjestrekning"))", FeatureType::Ferry },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE vegkategori IN ("E", "R"))", FeatureType::Big2 },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE vegkategori = "F")", FeatureType::Big },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE vegkategori IN ("K"))", FeatureType::Small2 },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE vegkategori IN ("P"))", FeatureType::Small },
  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE objtype IN ("Traktorveg"))", FeatureType::Gravel },
//  { "Basisdata_0000_Norge_25833_N50Kartdata.gpkg", R"(SELECT * FROM N50_Samferdsel_senterlinje WHERE objtype IN ("Sti", "GangSykkelveg", "Barmarksløype"))", FeatureType::Trail },

//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Tieluokka IN (1))"), FeatureType::Big2 },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Tieluokka IN (2, 3))"), FeatureType::Big },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Tieluokka IN (4, 5))"), FeatureType::Small2 },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Tieluokka IN (-29999) AND Paallyste IN (2))"), FeatureType::Small },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Paallyste IN (1))"), FeatureType::Gravel },
  { "mtkmaasto.gpkg", R"(SELECT * FROM TieViiva)", FeatureType::Trail },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Kohderyhma IN (25) AND Kohdeluokk IN (12111, 12112))"), FeatureType::Big2 },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Kohderyhma IN (25) AND Kohdeluokk IN (12121))"), FeatureType::Big },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Kohderyhma IN (25) AND Kohdeluokk IN (12122))"), FeatureType::Small2 },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Kohderyhma IN (25) AND Kohdeluokk IN (12131))"), FeatureType::Small },
//  { "finland.shp.zip", build_query("TieViiva", R"(WHERE Kohderyhma IN (25) AND Kohdeluokk IN (12132))"), FeatureType::Gravel },
//  { "finland.shp.zip", build_query("RautatieViiva"), FeatureType::Train },
});

struct File
{
  std::string file;
  GDALDatasetUniquePtr dataset;
};

static void renderLayers(auto const & layers_to_render, auto & files, png::image<png::gray_pixel> & image, uint8_t bit_offset, std::vector<double> adfProjection)
{
  std::vector<OGRLayerH> layers;
  std::vector<double> burnValues;

  for (auto const & l : layers_to_render)
  {
    if (auto const & file = std::ranges::find_if(files, [l](auto const & e){ return l.file == e.file; }); file != files.end())
    {
      layers.emplace_back(file->dataset->ExecuteSQL(l.query.c_str(), nullptr, nullptr));
    }
    else
    {
      files.emplace_back(File{
          l.file,
          GDALDatasetUniquePtr(GDALDataset::Open(l.file.c_str(), GDAL_OF_VECTOR)),
          });
      layers.emplace_back(files.back().dataset->ExecuteSQL(l.query.c_str(), nullptr, nullptr));
    }
    burnValues.push_back(static_cast<double>(static_cast<uint8_t>(l.type) << bit_offset));
  }

  GDALTransformerFunc pfnTransformer{};
  void *pTransformArg{};

  std::vector<char const *> papszOptions = {
    "ALL_TOUCHED=true",
    nullptr,
  };
  GDALProgressFunc pfnProgress{};
  void *pProgressArg{};

  int nBandCount = 1;
  auto poDriver = GetGDALDriverManager()->GetDriverByName("MEM");
  GDALDataset* poDstDS = poDriver->Create("dummy", image.get_width(), image.get_height(), nBandCount, GDT_Byte, nullptr);
//  poDstDS->SetGeoTransform(adfProjection);
  //files.front().dataset->GetGeoTransform(adfProjection);
  auto sweref = new OGRSpatialReference();
  //sweref->importFromEPSG(3006); // sweref99tm: https://epsg.io/3006
  sweref->importFromEPSG(3857); // web mercator: https://epsg.io/3857
  //sweref->importFromEPSG(3067); // tm35fin: https://epsg.io/3067

  poDstDS->SetSpatialRef(sweref);
  poDstDS->SetGeoTransform(adfProjection.data());
  std::vector<int> panBandList(nBandCount);
  std::fill(panBandList.begin(), panBandList.end(), 1);

  GDALRasterizeLayers(poDstDS, nBandCount, panBandList.data(), layers.size(),
      layers.data(), pfnTransformer, pTransformArg, burnValues.data(),
      (char **)papszOptions.data(), pfnProgress, pProgressArg);

  auto rasterBand = poDstDS->GetRasterBand(1);

  int nXBlockSize, nYBlockSize;

  rasterBand->GetBlockSize( &nXBlockSize, &nYBlockSize );
  int nXBlocks = (rasterBand->GetXSize() + nXBlockSize - 1) / nXBlockSize;
  int nYBlocks = (rasterBand->GetYSize() + nYBlockSize - 1) / nYBlockSize;

  for(int iYBlock = 0; iYBlock < nYBlocks; iYBlock++)
  {
    for(int iXBlock = 0; iXBlock < nXBlocks; iXBlock++)
    {
      auto block = rasterBand->GetLockedBlockRef(iXBlock, iYBlock);

      for(int y = 0; y < nYBlockSize; y++)
      {
        for(int x = 0; x < nXBlockSize; x++)
        {
          image[iYBlock * nYBlockSize + y][iXBlock * nXBlockSize + x] += static_cast<uint8_t*>(block->GetDataRef())[y * nYBlockSize + x];
        }
      }
      block->DropLock();
    }
  }

  /// REMOVE layers!

  GDALClose(poDstDS);
}

constexpr auto GRS80 = gis_cpp::Ellipsoid{
  .flattening = 1.0 / 298.257222101, .equatorialRadius = 6378137.0
};
constexpr auto SWEREF99TM = gis_cpp::Projection{
  .ellipsoid = GRS80, .centralMeridian = 15.0, .scale = 0.9996, .falseNorthing = 0.0, .falseEasting = 500000.0
};
constexpr auto TM35FIN = gis_cpp::Projection{
  .ellipsoid = GRS80, .centralMeridian = 27.0, .scale = 0.9996, .falseNorthing = 0.0, .falseEasting = 500000.0
};
constexpr auto WGS84 = gis_cpp::Ellipsoid{
  .flattening = 1.0 / 298.257223563, .equatorialRadius = 6378137.0
};
constexpr auto PseudoMercator = gis_cpp::Projection{
  .ellipsoid = WGS84, .centralMeridian = 0.0, .scale = 1.0, .falseNorthing = 20037508.34, .falseEasting = 20037508.34
};

constexpr auto Projection = PseudoMercator;
//constexpr auto Projection = SWEREF99TM;

std::string getTilePath(std::string const & dest, auto tile)
{
  auto n = static_cast<int>(std::pow(2, tile.zoom));

  std::string name;
  name += dest;
  name += "/";
  name += std::to_string(tile.zoom);
  name += "/";
  name += std::to_string(tile.column);
  name += "/";
  name += std::to_string(n - tile.row - 1);
  name += ".png";

  return name;
}

void generateZoomLevel(gis_cpp::Bounds<Projection> areaToProcess, unsigned int zoom, std::string const & dest)
{
  unsigned int numtiles = 128;
  unsigned int tileSize = 256;

  auto lowerleftTile = gis_cpp::tiling::gridToTile<Projection>(areaToProcess.min, zoom);
  auto upperRightTile = gis_cpp::tiling::gridToTile<Projection>(areaToProcess.max, zoom);

  std::cout << "Zoom level " << zoom << " tiles: column = " << lowerleftTile.column << " -> " << upperRightTile.column << ", row = " << lowerleftTile.row << " -> " << upperRightTile.row << " ====> (" << lowerleftTile.column << "," << lowerleftTile.row << ") -> (" << upperRightTile.column << "," << upperRightTile.row << ")\n";

  GDALAllRegister();

  std::vector<File> files;
  for (int row = lowerleftTile.row; row <= upperRightTile.row; row += numtiles)
  {
    for (int column = lowerleftTile.column; column <= upperRightTile.column; column += numtiles)
    {
      auto numx = std::min(upperRightTile.column - column, static_cast<int>(numtiles - 1));
      auto numy = std::min(upperRightTile.row - row, static_cast<int>(numtiles - 1));

      std::cout << " subtile: column = " << column << " -> " << column + numx << ", row = " << row << " -> " << row + numy << "\n";

      bool allExists = true;
      for (int iy = 0; iy <= numy; iy++)
        for (int ix = 0; ix <= numx; ix++)
          allExists &= std::filesystem::exists(getTilePath(dest, gis_cpp::Tile<Projection>{ row + iy, column + ix, zoom }));

      if (allExists)
      {
        std::cout << "   skipping\n";
        continue;
      }

      png::image<png::gray_pixel> image(tileSize * (numx + 1), tileSize * (numy + 1));

      auto minbounds = gis_cpp::tiling::tileBounds<Projection>({ .row = row, .column = column, .zoom = zoom }).min;
      auto maxbounds = gis_cpp::tiling::tileBounds<Projection>({ .row = row + numy, .column = column + numx, .zoom = zoom }).max;

//      std::cout << "TILEAREA bounds: min = " << minbounds.northing << "  " << minbounds.easting << ", max = " << maxbounds.northing << "  " << maxbounds.easting << "\n";

      std::vector<double> adfProjection = {
        minbounds.easting,
        (maxbounds.easting - minbounds.easting) / image.get_width(),
        0,
        maxbounds.northing,
        0,
        -(maxbounds.northing - minbounds.northing) / image.get_height()
      };

      std::cout << "    ";
      for(auto v : adfProjection)
        std::cout << v << ", ";
      std::cout << "\n";

//      std::cout << "  Rendering base\n";
//      renderLayers(baseLayers, files, image, 0, adfProjection);
      std::cout << "  Rendering features\n";
      renderLayers(featureLayers, files, image, 4, adfProjection);
      std::cout << "  Splitting image\n";

      png::image<png::gray_pixel> tileimage(tileSize, tileSize);

      for (int iy = 0; iy <= numy; iy++)
      {
        for (int ix = 0; ix <= numx; ix++)
        {
          unsigned int iyy = numy - iy;
          for (unsigned int y = 0; y < tileimage.get_height(); y++)
            for (unsigned int x = 0; x < tileimage.get_width(); x++)
              tileimage.set_pixel(x, y, image.get_pixel(ix * tileimage.get_width() + x, iyy * tileimage.get_height() + y));

          std::string name = getTilePath(dest, gis_cpp::Tile<Projection>{ row + iy, column + ix, zoom });
          std::filesystem::create_directories(std::filesystem::path(name).parent_path());
          tileimage.write(name);
        }
      }
    }
  }
}

int main(int argc, const char* argv[])
{
  std::cerr << gis_cpp::projection::geodeticToGrid<PseudoMercator>({90, 0}).northing << "\n";
  if (argc <= 6)
    return 1;

  gis_cpp::GeodeticCoord min = {std::strtod(argv[1], nullptr), std::strtod(argv[2], nullptr)};
  gis_cpp::GeodeticCoord max = {std::strtod(argv[3], nullptr), std::strtod(argv[4], nullptr)};
  unsigned int maxZoom = std::strtoul(argv[5], nullptr, 10);

  std::string dest = argv[6];

  std::cout << "INPUT bounds: min = " << min.latitude << " N, " << min.longitude << " E, max = " << max.latitude << " N, " << max.longitude << " E\n";

//  gis_cpp::Bounds<Projection> areaToProcess{
//    .min = gis_cpp::projection::geodeticToGrid<Projection>(min),
//    .max = gis_cpp::projection::geodeticToGrid<Projection>(max),
//  };

  gis_cpp::Bounds<Projection> areaToProcess{
    .min = {std::strtod(argv[1], nullptr), std::strtod(argv[2], nullptr)},
    .max = {std::strtod(argv[3], nullptr), std::strtod(argv[4], nullptr)},
  };

  std::cout << "AREA bounds: min = " << areaToProcess.min.northing << " N, " << areaToProcess.min.easting << " E, max = " << areaToProcess.max.northing << " N, " << areaToProcess.max.easting << " E\n";

  for (unsigned int z = 0; z <= maxZoom; z++)
    generateZoomLevel(areaToProcess, z, dest);

  return 0;
}
